class Cell {
    constructor(size, rowIndex, columnIndex) {
        this.height = size
        this.width = size
        this.columnIndex = columnIndex
        this.rowIndex = rowIndex
        this.cellElement = document.createElement('div')
 
        this.cellElement.id = columnIndex + ' ' + rowIndex
        this.cellElement.classList.add('cell')
    }
    swapStyles(newClass) {
        this.cellElement.className = newClass
    }
 }
