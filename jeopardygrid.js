class JeopardyGrid extends Grid {
    constructor(columns, rows, cellSize, categoryIDs, jeopardyCategoryURL = 'https://jservice.xyz/api/clues/?category=') {
        super(columns, rows, cellSize)
        this.categoryIDs = categoryIDs
        this.jeopardyCategoryURL = jeopardyCategoryURL
        this.playerPoints = 0
        this.submitButton = document.getElementById('submit')

        this.getCategories()
    }
    async getCategories() {
        const categoryPromises = this.categoryIDs.map(id => {
            return fetch(this.jeopardyCategoryURL + id).then(res => res.json())
        })
        this.categories = await Promise.all(categoryPromises)
        this.assignCategories()
    }
    assignCategories() {
        for (let i = 0; i < this.categories.length; i++) {
            let catHeader = this.createHeader(this.categories, i)
            let currentColumnElement = document.getElementById('column'+ i)
            currentColumnElement.prepend(catHeader)
            for(let j = 0; j < 5; j++){
                const clue = this.categories[i][j]
                let cellElement = this.cellGrid[i][j].cellElement
                let clueValue = clue.value
                let catQuestionText = document.createTextNode(clueValue)
                cellElement.appendChild(catQuestionText)
                this.boundClickHandler = this.unboundClickHandler.bind(this, clue, cellElement)
                cellElement.addEventListener('click', this.boundClickHandler)
            }
        }
    }
    createHeader(categories, categoryIndex) {
        let catHeader = document.createElement('div')
        catHeader.classList.add('header')
        catHeader.innerHTML = categories[categoryIndex][0].category.title

        return catHeader
    }
    unboundClickHandler(clue, cellElement) {
        let clueQuestion = clue.question
        let clueAnswer = clue.answer
        let clueValue = clue.value
        let questionElement = document.getElementById('question')

        this.submitButton.style.display = "block"
        questionElement.innerHTML = clueQuestion
        cellElement.innerHTML = ""
        cellElement.removeEventListener('click', this.boundClickHandler)
        this.boundCheckAnswer = this.unboundCheckAnswer.bind(this, clueAnswer, clueValue)
        this.submitButton.addEventListener('click', this.boundCheckAnswer)
    }
    unboundCheckAnswer(answer, value) {
        const scoreElement = document.getElementById('score')
        const userAnswerElement = document.getElementById('user-answer')
        const userAnswer = userAnswerElement.value
        

        if(userAnswer.toLowerCase() === answer.toLowerCase()) {
            this.playerPoints += Number(value)
            alert('Your answer is correct.')
            scoreElement.innerHTML = this.playerPoints
        } else{
            this.playerPoints -= Number(value)
            alert(`Your answer is incorrect. The correct answer is ${answer}.`)
            scoreElement.innerHTML = this.playerPoints
        }

        userAnswerElement.value = ''
        this.submitButton.removeEventListener('click', this.boundCheckAnswer)
        this.submitButton.style.display = 'none'
    }
}